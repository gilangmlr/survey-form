import React, { forwardRef } from 'react';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import MuiLink from '@material-ui/core/Link';

const NextComposed = (props) => {
  const { as, href, ...other } = props;

  return (
    <NextLink href={href} as={as}>
      <a {...other} />
    </NextLink>
  );
};

function Link(props, ref) {
  const {
    href,
    activeClassName = 'active',
    className: classNameProps,
    naked,
    ...other
  } = props;

  const router = useRouter();
  const pathname = typeof href === 'string' ? href : href.pathname;
  const className = clsx(classNameProps, {
    [activeClassName]: router.pathname === pathname && activeClassName,
  });

  if (naked) {
    return <NextComposed className={className} href={href} {...other} />;
  }

  return (
    <MuiLink
      ref={ref}
      component={NextComposed}
      className={className}
      href={href}
      {...other}
    />
  );
}

export default forwardRef(Link);
