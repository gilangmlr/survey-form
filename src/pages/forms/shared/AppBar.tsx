import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MUIAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { Container, Grid } from '@material-ui/core';
import { useRouter } from 'next/router';
import Link from '../../shared/Link';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function AppBar() {
  const router = useRouter();
  const { fid } = router.query;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <MUIAppBar position="fixed">
        <Toolbar>
          <Container maxWidth="md">
            <Grid container spacing={2}>
              <Grid item>
                <Button component={Link} naked href="/forms">
                  Forms
                </Button>
              </Grid>
              {!router.pathname.match(/\/(forms\/?)?$/) && (
                <>
                  <Grid item>
                    <Button
                      component={Link}
                      naked
                      href="/forms/[fid]/questions"
                      as={`/forms/${fid}/questions`}
                    >
                      Questions
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      component={Link}
                      naked
                      href="/forms/[fid]/responses"
                      as={`/forms/${fid}/responses`}
                    >
                      Responses
                    </Button>
                  </Grid>
                </>
              )}
            </Grid>
          </Container>
        </Toolbar>
      </MUIAppBar>
      <Toolbar />
    </div>
  );
}
