import React, { ReactNode } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
} from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import CloseIcon from '@material-ui/icons/Close';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import isEqual from 'lodash/isEqual';
import {
  FormComponentCardProps,
  FormComponent,
  FormComponentType,
  FormComponentOption,
} from '../../../@types';

const useStyles = makeStyles((theme) => ({
  layout: {
    paddingTop: theme.spacing(3),
  },
  formControl: {
    minWidth: 128,
  },
  formControlNumber: {
    width: 64,
    '& input': {
      textAlign: 'center',
    },
  },
  formControlOption: {
    minWidth: 512 - 40 - 46,
  },
  formControlText: {
    minWidth: 512,
  },
}));

const ComponentCard = ({
  index: componentIndex,
  componentsLength,
  component,
  setComponents,
}: FormComponentCardProps) => {
  const { type, title, maxChar, options } = component;

  const handleTextFieldFocus = (event) => {
    event.target.select();
  };

  const setComponent = (payload: Partial<FormComponent> | Function) => {
    setComponents((components) => [
      ...components.slice(0, componentIndex),
      {
        ...components[componentIndex],
        ...(typeof payload === 'function'
          ? payload(components[componentIndex])
          : payload),
      },
      ...components.slice(componentIndex + 1),
    ]);
  };

  const handleTypeChange = (event) => {
    const type = event.target?.value as FormComponentType;
    setComponent({ type });
  };

  const handleTitleChange = (event) => {
    const title = event.target?.value;
    setComponent({ title });
  };
  const handleMaxCharChange = (event) => {
    const maxChar = Number(event.target?.value);
    setComponent({ maxChar });
  };

  const setOptions = (payload: Array<FormComponentOption> | Function) =>
    setComponent((component) => ({
      ...component,
      options:
        typeof payload === 'function' ? payload(component.options) : payload,
    }));

  const handleAddOptionClick = () =>
    setOptions((options) => [
      ...options,
      {
        ts: new Date().getTime(),
        label: `Option ${options.length + 1}`,
      },
    ]);

  const handleOptionLabelChange = (i) => (event) => {
    const label = event.target?.value;
    setOptions((options) => [
      ...options.slice(0, i),
      {
        ...options[i],
        label,
      },
      ...options.slice(i + 1),
    ]);
  };

  const handleRemoveOptionClick = (i) => () =>
    setOptions((options) => [...options.slice(0, i), ...options.slice(i + 1)]);

  const handleRemoveComponentClick = () =>
    setComponents((components) => [
      ...components.slice(0, componentIndex),
      ...components.slice(componentIndex + 1),
    ]);

  const swapComponent = (i, j) => () => {
    setComponents((components) => [
      ...components.slice(0, i),
      components[j],
      components[i],
      ...components.slice(j + 1),
    ]);
  };

  const classes = useStyles();

  const multipleChoicesForm = (type: FormComponentType) => (
    <Grid container spacing={2} direction="column">
      <Grid item>
        <FormControl className={classes.formControlText}>
          <TextField
            label="Component"
            value={title}
            onChange={handleTitleChange}
            onFocus={handleTextFieldFocus}
          />
        </FormControl>
      </Grid>
      <Grid item>
        <Grid container spacing={2} direction="column">
          {options.map(({ ts, label = '' }, i) => (
            <Grid key={`${ts}-${i}`} item>
              <Grid container spacing={2} direction="row" alignItems="center">
                {type === 'RADIO' ? (
                  <Grid item>
                    <RadioButtonUncheckedIcon />
                  </Grid>
                ) : (
                  <Grid item>{`${i + 1}.`}</Grid>
                )}
                <Grid item>
                  <FormControl className={classes.formControlOption}>
                    <TextField
                      label={`Option ${i + 1}`}
                      value={label}
                      onChange={handleOptionLabelChange(i)}
                      onFocus={handleTextFieldFocus}
                    />
                  </FormControl>
                </Grid>
                <Grid item>
                  <IconButton size="small" onClick={handleRemoveOptionClick(i)}>
                    <CloseIcon />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          ))}
          <Grid item>
            <Grid container spacing={2} direction="row" alignItems="center">
              {type === 'RADIO' ? (
                <Grid item>
                  <RadioButtonUncheckedIcon />
                </Grid>
              ) : (
                <Grid item>{`${options.length + 1}.`}</Grid>
              )}
              <Grid item>
                <Button onClick={handleAddOptionClick}>Add option</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );

  const componentTypeForm: Record<FormComponentType, ReactNode> = {
    STATIC: (
      <FormControl className={classes.formControlText}>
        <TextField
          label="Text"
          multiline
          rows={4}
          value={title}
          onChange={handleTitleChange}
          onFocus={handleTextFieldFocus}
        />
      </FormControl>
    ),
    TEXT: (
      <Grid container spacing={2} direction="column">
        <Grid item>
          <FormControl className={classes.formControlText}>
            <TextField
              label="Component"
              value={title}
              onChange={handleTitleChange}
              onFocus={handleTextFieldFocus}
            />
          </FormControl>
        </Grid>
        <Grid item>
          <Grid
            container
            spacing={2}
            direction="row"
            justify="flex-start"
            alignItems="center"
          >
            <Grid item>
              <Typography>Allowed characters</Typography>
            </Grid>
            <Grid item>
              <FormControl className={classes.formControlNumber}>
                <TextField
                  type="number"
                  value={maxChar}
                  onChange={handleMaxCharChange}
                  onFocus={handleTextFieldFocus}
                />
              </FormControl>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    ),
    RADIO: multipleChoicesForm('RADIO'),
    DROPDOWN: multipleChoicesForm('DROPDOWN'),
  };

  return (
    <div className={classes.layout}>
      <Grid container spacing={2} direction="row" alignItems="center">
        <Grid item xs={11}>
          <Card>
            <CardContent>
              <Grid container spacing={1}>
                <Grid item xs={3}>
                  <FormControl className={classes.formControl}>
                    <InputLabel id="demo-simple-select-label">Type</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={type}
                      onChange={handleTypeChange}
                    >
                      <MenuItem value="STATIC">Static</MenuItem>
                      <MenuItem value="TEXT">Text</MenuItem>
                      <MenuItem value="RADIO">Radio</MenuItem>
                      <MenuItem value="DROPDOWN">Dropdown</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={9}>
                  {componentTypeForm[type]}
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>

        <Grid item xs={1}>
          <Paper>
            <Grid
              container
              spacing={1}
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                <IconButton
                  size="small"
                  disabled={componentIndex === 0}
                  onClick={swapComponent(componentIndex - 1, componentIndex)}
                >
                  <ArrowUpwardIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton
                  size="small"
                  disabled={componentIndex === componentsLength - 1}
                  onClick={swapComponent(componentIndex, componentIndex + 1)}
                >
                  <ArrowDownwardIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton size="small" onClick={handleRemoveComponentClick}>
                  <CloseIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default React.memo(ComponentCard, (prevProps, nextProps) =>
  isEqual(prevProps, nextProps)
);
