import React, { ReactNode } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {
  FormControl,
  FormControlLabel,
  Grid,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField,
} from '@material-ui/core';
import isEqual from 'lodash/isEqual';
import {
  FormComponentType,
  FormComponentResponse,
  FormComponentViewCardProps,
} from '../../../@types';

const useStyles = makeStyles((theme) => ({
  layout: {
    paddingTop: theme.spacing(3),
  },
  formControl: {
    minWidth: 256,
    maxWidth: 720,
  },
  formControlNumber: {
    width: 64,
    '& input': {
      textAlign: 'center',
    },
  },
  formControlOption: {
    minWidth: 512 - 40 - 46,
  },
  formControlText: {
    minWidth: 720,
  },
}));

const ComponentCard = ({
  index: componentIndex,
  component,
  setComponents,
  readOnly,
}: FormComponentViewCardProps) => {
  const { type, title, maxChar, options, response } = component;

  const handleTextFieldFocus = (event) => {
    event.target.select();
  };

  const setComponent = (payload: Partial<FormComponentResponse> | Function) => {
    setComponents((components) => [
      ...components.slice(0, componentIndex),
      {
        ...components[componentIndex],
        ...(typeof payload === 'function'
          ? payload(components[componentIndex])
          : payload),
      },
      ...components.slice(componentIndex + 1),
    ]);
  };

  const handleResponseChange = (event) => {
    if (readOnly) return;
    const response = event.target?.value;
    if (type === 'TEXT' && response.length > maxChar) return;
    setComponent({ response });
  };

  const classes = useStyles();

  const multipleChoicesForm = (type: FormComponentType) => (
    <Grid container spacing={2} direction="column">
      <Grid item>
        <Typography>{title}</Typography>
      </Grid>
      <Grid item>
        {type === 'RADIO' ? (
          <FormControl>
            <RadioGroup value={response} onChange={handleResponseChange}>
              {options.map(({ ts, label }, i) => (
                <FormControlLabel
                  key={`${ts}-${i}`}
                  value={label}
                  control={<Radio />}
                  label={label}
                />
              ))}
            </RadioGroup>
          </FormControl>
        ) : (
          <FormControl className={classes.formControl}>
            <Select
              value={response}
              onChange={handleResponseChange}
              label="Choose"
              readOnly={readOnly}
            >
              {options.map(({ ts, label }, i) => (
                <MenuItem key={`${ts}-${i}`} value={label}>
                  {label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
      </Grid>
    </Grid>
  );

  const componentTypeForm: Record<FormComponentType, ReactNode> = {
    STATIC: <Typography>{title}</Typography>,
    TEXT: (
      <Grid container spacing={2} direction="column">
        <Grid item>
          <Typography>{title}</Typography>
        </Grid>
        <Grid item>
          <FormControl className={classes.formControlText}>
            <TextField
              label="Your answer"
              value={response}
              multiline
              rowsMax={4}
              onChange={handleResponseChange}
              onFocus={handleTextFieldFocus}
              InputProps={{
                readOnly: true,
              }}
            />
          </FormControl>
        </Grid>
        <Grid item>
          {response.length}/{maxChar}
        </Grid>
      </Grid>
    ),
    RADIO: multipleChoicesForm('RADIO'),
    DROPDOWN: multipleChoicesForm('DROPDOWN'),
  };

  return (
    <div className={classes.layout}>
      <Card>
        <CardContent>
          <Grid container>
            <Grid item xs={12}>
              {componentTypeForm[type]}
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </div>
  );
};

export default React.memo(ComponentCard, (prevProps, nextProps) =>
  isEqual(prevProps, nextProps)
);
