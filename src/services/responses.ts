import { InsertOneWriteOpResult } from 'mongodb';
import { FormResponse } from '../../@types';
import getAxios from '../shared/axios';

export const createResponse = async (
  formResponse: Omit<FormResponse, '_id'>
): Promise<InsertOneWriteOpResult<FormResponse>> => {
  const axios = getAxios();
  const res = await axios.post('/api/responses', formResponse);
  const { data } = res;
  return data.data;
};

export const getResponses = async (fid): Promise<Array<FormResponse>> => {
  const axios = getAxios();
  const res = await axios.get(`/api/responses/forms/${fid}`);
  const { data } = res;
  return data.data as Array<FormResponse>;
};
