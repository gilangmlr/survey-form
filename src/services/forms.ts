import { InsertOneWriteOpResult, UpdateWriteOpResult } from 'mongodb';
import { Form } from '../../@types';
import getAxios from '../shared/axios';

export const createForm = async (
  form: Omit<Form, '_id'>
): Promise<InsertOneWriteOpResult<Form>> => {
  const axios = getAxios();
  const res = await axios.post('/api/forms', form);
  const { data } = res;
  return data.data;
};

export const getForms = async (): Promise<Array<Form>> => {
  const axios = getAxios();
  const res = await axios.get('/api/forms');
  const { data } = res;
  return data.data as Array<Form>;
};

export const getForm = async (_id): Promise<Form> => {
  const axios = getAxios();
  const res = await axios.get(`/api/forms/${_id}`);
  const { data } = res;
  return data.data || ({} as Form);
};

export const updateForm = async ({
  _id,
  form,
}: {
  _id: any;
  form: Omit<Form, '_id'>;
}): Promise<UpdateWriteOpResult> => {
  const axios = getAxios();
  const res = await axios.patch(`/api/forms/${_id}`, form);
  const { data } = res;
  return data.data;
};

export const deleteForm = async (_id: string): Promise<Form> => {
  const axios = getAxios();
  const res = await axios.delete(`/api/forms/${_id}`);
  const { data } = res;
  return data.data || ({} as Form);
};
