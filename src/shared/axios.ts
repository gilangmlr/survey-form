import axios from 'axios';

const getAxios = () => axios.create();

export default getAxios;
