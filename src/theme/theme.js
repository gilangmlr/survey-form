import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      light: '#ffa040',
      main: '#ff6f00',
      dark: '#c43e00',
    },
    secondary: {
      light: '#006064',
      main: '#428e92',
      dark: '#00363a',
    },
  },
});

export default theme;
