import { Db, MongoClient } from 'mongodb';
import { NextApiRequest } from 'next';

declare type FormComponentType = 'STATIC' | 'TEXT' | 'RADIO' | 'DROPDOWN';

declare interface FormComponentOption {
  ts: number;
  label: string;
}

declare interface FormComponent {
  ts: number;
  type: FormComponentType;
  title: string;
  maxChar: number;
  options: Array<FormComponentOption>;
}

declare interface FormComponentCardProps {
  index: number;
  componentsLength: number;
  component: FormComponent;
  setComponents: Function;
}

declare interface FormComponentViewCardProps extends FormComponentCardProps {
  component: FormComponentResponse;
  componentsLength?: number;
  setComponents?: Function;
  readOnly?: boolean;
}

declare interface Form {
  _id: any;
  title: string;
  description: string;
  components: Array<FormComponent>;
}

declare interface FormComponentResponse extends FormComponent {
  response: string;
}

declare interface FormResponse extends Form {
  formId: any;
  components: Array<FormComponentResponse>;
}

declare interface MongoNextApiRequest extends NextApiRequest {
  dbClient: MongoClient;
  db: Db;
}
