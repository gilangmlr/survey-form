This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Environment Variables Setup

Create a file named `.env.local` to store the environment variables needed to connect to a MongoDB instance.

```
MONGODB_USER=dbUser
MONGODB_PASSWORD=dbUserPassword
MONGODB_DATABASE=dbName
MONGODB_CONNECTION_URI='mongodb://$MONGODB_USER:$MONGODB_PASSWORD@localhost:27017/$MONGODB_DATABASE?retryWrites=true&w=majority'
```

## Development Mode

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Production Mode

To run the application in production mode, run these commands:

```bash
yarn build
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
