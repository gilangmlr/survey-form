import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CircularProgress,
  Container,
  Grid,
  IconButton,
  makeStyles,
  Tooltip,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import AppBar from '../../src/pages/forms/shared/AppBar';
import { createForm, deleteForm, getForms } from '../../src/services/forms';

const FormCard = ({ form, formDidRemove }) => {
  const [isRemovingForm, setIsRemovingForm] = useState(false);

  const router = useRouter();

  const handleRemoveFormClick = (_id) => async () => {
    setIsRemovingForm(true);
    await deleteForm(_id);
    setIsRemovingForm(false);
    formDidRemove();
  };

  const handleFormCardClick = (_id) => () => {
    router.push(`/forms/${_id}/questions`);
  };

  const handleOpenInNewClick = (_id) => () => {
    window.open(`/forms/${_id}/questions`, '_blank');
  };

  return (
    <Card>
      <CardActionArea onClick={handleFormCardClick(form._id)}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {form.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {form.description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions disableSpacing>
        <Tooltip title="Remove">
          <IconButton
            aria-label="remove"
            onClick={handleRemoveFormClick(form._id)}
            disabled={isRemovingForm}
          >
            <DeleteIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Open in new tab">
          <IconButton
            aria-label="open in new tab"
            onClick={handleOpenInNewClick(form._id)}
          >
            <OpenInNewIcon />
          </IconButton>
        </Tooltip>
      </CardActions>
    </Card>
  );
};

export default function Forms() {
  const [forms, setForms] = useState([]);
  const [isAddingForm, setIsAddingForm] = useState(false);
  const [isGettingForms, setIsGettingForms] = useState(true);

  const router = useRouter();
  const classes = useStyles();

  const handleFormDidRemove = async () => {
    const forms = await getForms();
    setForms(forms);
  };

  const handleAddFormClick = async () => {
    setIsAddingForm(true);
    const { insertedId } = await createForm({
      title: 'Untitled form',
      description: 'Form description',
      components: [],
    });
    router.push(`/forms/${insertedId}`);
    setIsAddingForm(false);
  };

  useEffect(() => {
    (async () => {
      setIsGettingForms(true);
      const forms = await getForms();
      setForms(forms);
      setIsGettingForms(false);
    })();
  }, []);

  return (
    <Container maxWidth="md">
      <AppBar />
      <div className={classes.layout}>
        <div className={classes.actions}>
          <Button
            variant="outlined"
            size="large"
            startIcon={<AddIcon />}
            onClick={handleAddFormClick}
            disabled={isAddingForm}
          >
            Add Form
          </Button>
        </div>

        {isGettingForms ? (
          <Grid
            container
            spacing={2}
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item xs={12}>
              <CircularProgress />
            </Grid>
          </Grid>
        ) : (
          <Grid container spacing={2}>
            {forms.map((form, i) => (
              <Grid key={i} item xs={12} sm={6} md={4} lg={3}>
                <FormCard form={form} formDidRemove={handleFormDidRemove} />
              </Grid>
            ))}
          </Grid>
        )}
      </div>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  layout: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  actions: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
}));
