import {
  Card,
  CardContent,
  CircularProgress,
  Container,
  Grid,
  IconButton,
  makeStyles,
  Typography,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import ComponentViewCard from '../../../src/pages/forms/ComponentViewCard';
import AppBar from '../../../src/pages/forms/shared/AppBar';
import { getResponses } from '../../../src/services/responses';

const useStyles = makeStyles((theme) => ({
  layout: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  bottomActions: {
    marginTop: theme.spacing(3),
  },
  inputFile: {
    display: 'none',
  },
  formControlText: {
    minWidth: 512,
  },
  headerLayout: {
    paddingTop: theme.spacing(3),
  },
}));

const ResponseDetail = ({ response }) => {
  const { title, description, components } = response;

  const classes = useStyles();

  const router = useRouter();

  return (
    <Container maxWidth="md">
      <div className={classes.headerLayout}>
        <Card>
          <CardContent>
            <Typography variant="h4" gutterBottom>
              {title}
            </Typography>
            <Typography variant="body2">{description}</Typography>
          </CardContent>
        </Card>
      </div>

      {components.map((component, i) => (
        <ComponentViewCard
          key={`${component?.ts}-${i}`}
          index={i}
          componentsLength={components.length}
          component={component}
          readOnly
        />
      ))}
    </Container>
  );
};

export default function Responses() {
  const [responses, setResponses] = useState([]);
  const [selectedResponseIndex, setSelectedResponseIndex] = useState(0);
  const [isGettingResponses, setIsGettingResponses] = useState(true);

  const classes = useStyles();

  const router = useRouter();
  const { fid } = router.query;

  const handlePrevResponseClick = () => {
    setSelectedResponseIndex(
      (selectedResponseIndex) => selectedResponseIndex - 1
    );
  };

  const handleNextResponseClick = () => {
    setSelectedResponseIndex(
      (selectedResponseIndex) => selectedResponseIndex + 1
    );
  };

  useEffect(() => {
    if (!fid) return;

    (async () => {
      setIsGettingResponses(true);
      const responses = await getResponses(fid);
      setResponses(responses || []);
      setIsGettingResponses(false);
    })();
  }, [fid]);

  return (
    <Container>
      <AppBar />
      <div className={classes.layout}>
        <Container maxWidth="md">
          {isGettingResponses ? (
            <div className={classes.headerLayout}>
              <Grid
                container
                spacing={2}
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item xs={12}>
                  <CircularProgress />
                </Grid>
              </Grid>
            </div>
          ) : responses.length > 0 ? (
            <>
              <Grid container spacing={1} justify="center" alignItems="center">
                <Grid item>
                  <IconButton
                    onClick={handlePrevResponseClick}
                    disabled={selectedResponseIndex === 0}
                    size="small"
                  >
                    <ChevronLeftIcon />
                  </IconButton>
                </Grid>
                <Grid item>
                  <Typography variant="h6">
                    {selectedResponseIndex + 1}/{responses.length}
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    onClick={handleNextResponseClick}
                    disabled={selectedResponseIndex === responses.length - 1}
                    size="small"
                  >
                    <ChevronRightIcon />
                  </IconButton>
                </Grid>
              </Grid>
              <ResponseDetail response={responses[selectedResponseIndex]} />
            </>
          ) : (
            <div className={classes.headerLayout}>
              <Typography variant="h4">No responses</Typography>
            </div>
          )}
        </Container>
      </div>
    </Container>
  );
}
