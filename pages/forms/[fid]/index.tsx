import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  Container,
  FormControl,
  Grid,
  makeStyles,
  TextField,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';
import debounce from 'lodash/debounce';
import omit from 'lodash/omit';

import ComponentCard from '../../../src/pages/forms/ComponentCard';
import AppBar from '../../../src/pages/forms/shared/AppBar';
import { getForm, updateForm } from '../../../src/services/forms';

const useStyles = makeStyles((theme) => ({
  layout: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  bottomActions: {
    marginTop: theme.spacing(3),
  },
  inputFile: {
    display: 'none',
  },
  formControlText: {
    minWidth: 512,
  },
  headerLayout: {
    paddingTop: theme.spacing(3),
  },
}));

export type Header = {
  _id?: string;
  title: string;
  description: string;
};

export type FormHeaderProps = { header: Header; setHeader: Function };

const debouncedUpdateForm = debounce(updateForm, 250);

const FormHeader = ({ header, setHeader }: FormHeaderProps) => {
  const { title, description } = header;
  const classes = useStyles();

  const handleTextFieldFocus = (event) => event.target.select();
  const handleTitleChange = (event) => {
    const title = event.target.value;
    setHeader((header) => ({
      ...header,
      title,
    }));
  };
  const handleDescriptionChange = (event) => {
    const description = event.target.value;
    setHeader((header) => ({
      ...header,
      description,
    }));
  };

  return (
    <div className={classes.headerLayout}>
      <Card>
        <CardContent>
          <Grid container spacing={2} direction="column">
            <Grid item>
              <FormControl className={classes.formControlText}>
                <TextField
                  label="Form title"
                  value={title}
                  onChange={handleTitleChange}
                  onFocus={handleTextFieldFocus}
                />
              </FormControl>
            </Grid>
            <Grid item>
              <FormControl className={classes.formControlText}>
                <TextField
                  label="Form description"
                  multiline
                  rows={2}
                  value={description}
                  onChange={handleDescriptionChange}
                  onFocus={handleTextFieldFocus}
                />
              </FormControl>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </div>
  );
};

export default function FormDetail() {
  const [header, setHeader] = useState({
    _id: '',
    title: '',
    description: '',
  });
  const [components, setComponents] = useState([]);
  const [isGettingForm, setIsGettingForm] = useState(true);

  const classes = useStyles();

  const shouldUpdateForm = useRef(true);

  const router = useRouter();
  const { fid } = router.query;

  const handleUploadChange = (event) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      const { title, description, components } = JSON.parse(
        event.target.result as string
      );
      setHeader((header) => ({
        ...header,
        title,
        description,
      }));
      setComponents(components);
    };
    try {
      reader.readAsText(event.target.files[0]);
    } catch (error) {}
  };

  const handleUploadClick = () => {
    document.getElementById('input-json-file').click();
  };

  const handleDownloadClick = () => {
    const dataStr =
      'data:text/json;charset=utf-8,' +
      encodeURIComponent(
        JSON.stringify(
          {
            ...header,
            components,
          },
          null,
          2
        )
      );
    const downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute('href', dataStr);
    downloadAnchorNode.setAttribute(
      'download',
      `${header.title}_SurveyForm.json`
    );
    document.body.appendChild(downloadAnchorNode);
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
  };

  const handleViewFormClick = () => {
    window.open(`/forms/${header._id}/viewform`, '_blank');
  };

  useEffect(() => {
    if (!shouldUpdateForm.current || !header._id) {
      shouldUpdateForm.current = true;
      return;
    }
    debouncedUpdateForm({
      _id: header._id,
      form: {
        ...omit(header, '_id'),
        components,
      },
    });
  }, [header, components]);

  useEffect(() => {
    if (!fid) return;

    (async () => {
      setIsGettingForm(true);
      const { _id, title, description, components } = await getForm(fid);
      setHeader({
        _id,
        title,
        description,
      });
      setComponents(components || []);
      setIsGettingForm(false);
    })();
  }, [fid]);

  return (
    <Container>
      <AppBar />
      <div className={classes.layout}>
        <Container maxWidth="md">
          <Grid container>
            <Grid item xs={6}>
              <Grid container spacing={2}>
                <Grid item>
                  <input
                    accept="application/json"
                    className={classes.inputFile}
                    id="input-json-file"
                    type="file"
                    onChange={handleUploadChange}
                  />
                  <Button
                    variant="outlined"
                    startIcon={<CloudUploadIcon />}
                    size="large"
                    onClick={handleUploadClick}
                  >
                    Upload JSON
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant="outlined"
                    startIcon={<CloudDownloadIcon />}
                    size="large"
                    onClick={handleDownloadClick}
                  >
                    Download JSON
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <Grid container direction="row-reverse">
                <Button
                  variant="outlined"
                  startIcon={<VisibilityIcon />}
                  size="large"
                  onClick={handleViewFormClick}
                >
                  View Form
                </Button>
              </Grid>
            </Grid>
          </Grid>

          {isGettingForm ? (
            <div className={classes.headerLayout}>
              <Grid
                container
                spacing={2}
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item xs={12}>
                  <CircularProgress />
                </Grid>
              </Grid>
            </div>
          ) : (
            <>
              <FormHeader header={header} setHeader={setHeader} />

              {components.map((component, i) => (
                <ComponentCard
                  key={`${component?.ts}-${i}`}
                  index={i}
                  componentsLength={components.length}
                  component={component}
                  setComponents={setComponents}
                />
              ))}
            </>
          )}

          <div className={classes.bottomActions}>
            <Button
              variant="outlined"
              size="large"
              startIcon={<AddIcon />}
              onClick={() =>
                setComponents((components) => [
                  ...components,
                  {
                    ts: new Date().getTime(),
                    type: 'STATIC',
                    title: 'Component',
                    maxChar: 500,
                    options: [],
                  },
                ])
              }
            >
              Add Component
            </Button>
          </div>
        </Container>
      </div>
    </Container>
  );
}
