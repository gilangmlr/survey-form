import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  Container,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import { getForm } from '../../../src/services/forms';
import ComponentViewCard from '../../../src/pages/forms/ComponentViewCard';
import { createResponse } from '../../../src/services/responses';

const useStyles = makeStyles((theme) => ({
  layout: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  bottomActions: {
    marginTop: theme.spacing(3),
  },
  inputFile: {
    display: 'none',
  },
  formControlText: {
    minWidth: 512,
  },
  headerLayout: {
    paddingTop: theme.spacing(3),
  },
}));

export type Header = {
  _id?: string;
  title: string;
  description: string;
};

export default function FormDetail() {
  const [header, setHeader] = useState({
    formId: '',
    title: '',
    description: '',
  });
  const [components, setComponents] = useState([]);
  const [isGettingForm, setIsGettingForm] = useState(true);
  const [isSubmittingForm, setIsSubmittingForm] = useState(false);
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);

  const classes = useStyles();

  const router = useRouter();
  const { fid } = router.query;

  const handleSubmitClick = () => {
    setIsSubmittingForm(true);
    createResponse({
      ...header,
      components,
    });
    setIsSubmittingForm(false);
    setIsFormSubmitted(true);
  };

  const handleSubmitAnotherClick = () => {
    router.reload();
  };

  useEffect(() => {
    if (!fid) return;

    (async () => {
      setIsGettingForm(true);
      const { _id, title, description, components } = await getForm(fid);
      setHeader({
        formId: _id,
        title,
        description,
      });
      setComponents(
        (components || []).map((component) => ({ ...component, response: '' }))
      );
      setIsGettingForm(false);
    })();
  }, [fid]);

  return (
    <Container>
      <div className={classes.layout}>
        <Container maxWidth="md">
          {isGettingForm ? (
            <div className={classes.headerLayout}>
              <Grid
                container
                spacing={2}
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item xs={12}>
                  <CircularProgress />
                </Grid>
              </Grid>
            </div>
          ) : isFormSubmitted ? (
            <>
              <div className={classes.headerLayout}>
                <Card>
                  <CardContent>
                    <Typography variant="h4" gutterBottom>
                      {header.title}
                    </Typography>
                    <Typography variant="body2">
                      Your response has been recorded.
                    </Typography>
                  </CardContent>
                </Card>
              </div>
              <div className={classes.bottomActions}>
                <Button variant="outlined" onClick={handleSubmitAnotherClick}>
                  Submit another response
                </Button>
              </div>
            </>
          ) : (
            <>
              <div className={classes.headerLayout}>
                <Card>
                  <CardContent>
                    <Typography variant="h4" gutterBottom>
                      {header.title}
                    </Typography>
                    <Typography variant="body2">
                      {header.description}
                    </Typography>
                  </CardContent>
                </Card>
              </div>

              {components.map((component, i) => (
                <ComponentViewCard
                  key={`${component?.ts}-${i}`}
                  index={i}
                  componentsLength={components.length}
                  component={component}
                  setComponents={setComponents}
                />
              ))}

              <div className={classes.bottomActions}>
                <Button
                  variant="outlined"
                  size="large"
                  onClick={handleSubmitClick}
                  disabled={isSubmittingForm}
                >
                  Submit
                </Button>
              </div>
            </>
          )}
        </Container>
      </div>
    </Container>
  );
}
