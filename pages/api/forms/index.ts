import { NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import middleware from '../../../src/middleware/database';
import { MongoNextApiRequest } from '../../../@types';

const handler = nextConnect();

handler.use(middleware);

handler.get(async (req: MongoNextApiRequest, res: NextApiResponse) => {
  const docs = await req.db.collection('forms').find({}).toArray();
  res.json({ data: docs });
});

handler.post(async (req: MongoNextApiRequest, res: NextApiResponse) => {
  const data = req.body;
  const opRes = await req.db.collection('forms').insertOne(data);

  res.json({ data: opRes });
});

export default (req, res) => handler.apply(req, res);
