import { NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import middleware from '../../../src/middleware/database';
import { MongoNextApiRequest } from '../../../@types';
import { ObjectID } from 'mongodb';

const handler = nextConnect();

handler.use(middleware);

handler.get(async (req: MongoNextApiRequest, res: NextApiResponse) => {
  const { fid } = req.query;
  const _id = new ObjectID(fid as string);

  const doc = await req.db.collection('forms').findOne({ _id });
  res.json({ data: doc });
});

handler.patch(async (req: MongoNextApiRequest, res: NextApiResponse) => {
  const { fid } = req.query;
  const _id = new ObjectID(fid as string);
  const data = req.body;
  const opRes = await req.db
    .collection('forms')
    .updateOne({ _id }, { $set: data });
  res.json({ data: opRes });
});

handler.delete(async (req: MongoNextApiRequest, res: NextApiResponse) => {
  const { fid } = req.query;
  const _id = new ObjectID(fid as string);
  const opRes = await req.db.collection('forms').deleteOne({ _id });
  res.json({ data: opRes });
});

export default (req, res) => handler.apply(req, res);
