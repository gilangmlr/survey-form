import { NextApiResponse } from 'next';
import nextConnect from 'next-connect';
import middleware from '../../../../src/middleware/database';
import { MongoNextApiRequest } from '../../../../@types';

const handler = nextConnect();

handler.use(middleware);

handler.get(async (req: MongoNextApiRequest, res: NextApiResponse) => {
  const { fid } = req.query;
  const docs = await req.db
    .collection('responses')
    .find({ formId: fid })
    .toArray();
  res.json({ data: docs });
});

export default (req, res) => handler.apply(req, res);
